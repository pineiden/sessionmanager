from sqlalchemy import create_engine

try:
    from models import Base
except Exception:
    from .models import Base

try:
    from db_session import engine
except Exception:
    from .db_session import engine

def create_db(engine):
    """
    Create the SQL Schema and execute to build the database
    """
    Base.metadata.create_all(engine)

create_db(engine)
