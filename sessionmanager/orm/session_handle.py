from sqlalchemy import update
from sqlalchemy import Table, MetaData
from saltpepper.nosql.manager import RethinkDBPepper

try:
    from .db_session import session, engine, connection
except Exception:
    from db_session import session, engine, connection
try:
    from .models import Level, User, Command
except Exception:
    from models import Level, User, Command

import datetime

try:
    from .password import PasswordHash
except Exception:
    from password import PasswordHash


import asyncio

class SessionHandle:
    """
    A session middleware class to manage the basic elements in the database,
    has generic methods to verify the elements existence, update_X and obtain
    lists from tables
    """

    def __init__(self):
        self.session = session
        self.conn = connection
        self.metadata = MetaData()
        self.pepper = RethinkDBPepper()
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(self.pepper.create_db_table())

    def close(self):
        self.session.close()
        self.pepper.close()

    async def create_salt_table(self):
        #self.pepper.up_session()
        #await self.pepper.rethinkdb.reconnect()
        await self.pepper.create_db_table()

    async def generate_salt(self, user):
        return await self.pepper.generate_salt(user)

    async def update_salt(self, user):
        #self.pepper.up_session()
        #await self.pepper.rethinkdb.reconnect()
        await self.pepper.update_salt(user)

    async def get_salt(self, user):
        #self.pepper.up_session()
        #await self.pepper.rethinkdb.reconnect()
        return await self.pepper.get_salt(user)

    async def delete_salt(self, user):
        #self.pepper.up_session()
        #await self.pepper.rethinkdb.reconnect()
        return await self.pepper.delete_salt(user)


    def exists_table(self, table_name):
        """
        Check if table_name exists on schema
        :param table_name: a table_name string
        :return: boolean {True,False}
        """
        return engine.dialect.has_table(engine.connect(), table_name)

    def exists_field(self, table_name, field_name):
        """
        Check if field exist in table
        :param table_name: table name string
        :param field_name: field name string
        :return:  bolean {True, False}
        """
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        r = [c.name for c in fields.columns]
        try:
            r.remove('id')
        except ValueError:
            pass
        assert field_name in r, 'No existe este campo en tabla'
        return True

    def value_type(self, table_name, field_name, value):
        """
        Check if value is the same value type in field

        :param table_name: table name string
        :param field_name: field name string
        :param value:  some value
        :return: boolean {True, False, None}; None if value type doesn't exist
        on that list, because there are only the most common types.
        """
        # get type value
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        r = [c.name for c in fields.columns]
        assert self.exists_field(table_name, field_name), \
            'Campo no existe en tabla'
        this_index = r.index(field_name)
        t = [str(c.type) for c in fields.columns]
        this_type = t[this_index]
        b = False
        if this_type == 'INTEGER' or this_type == 'BigInteger':
            assert isinstance(value, int)
            b = True
        elif this_type[
                0:
                7] == 'VARCHAR' or this_type == 'TEXT' or this_type == 'STRING':
            assert isinstance(value, str)
            b = True
        elif this_type == 'BOOLEAN':
            assert isinstance(value, bool)
            b = True
        elif this_type == 'DATE':
            assert isinstance(value, datetime.date)
            b = True
        elif this_type == 'DATETIME':
            assert isinstance(value, datetime.datetime)
            b = True
        elif this_type == 'FLOAT' or this_type == 'NUMERIC':
            assert isinstance(value, float)
            b = True
        else:
            b = None

        return b
        # check

    def update_table(self, table_name, instance, field_name, value):
        """
        Change some value in table

        :param table_name: table name class
        :param instance: instance to modify in database
        :param field: field name string
        :param value: value
        :returns: void()
        """
        table = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        print("Instance -> ")
        print(instance)
        print(instance.id)
        print("Table")
        print(table)
        up = update(table).where(
            table.c.id == instance.id).values({field_name: value})
        print(up)
        self.session.execute(up)
        self.session.commit()

    # LIST ELEMENTS

    def get_list(self, model):
        """
        Get the complete list of elements in some Model Class (Table in db)

        :param model:Model Class
        :returns: a query list
        """
        return self.session.query(model).all()


