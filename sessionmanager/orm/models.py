from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates
from sqlalchemy import Column, Integer, Text, String, DateTime, ForeignKey, Boolean
try:
    from validators import isPositive
except Exception:
    from .validators import isPositive
# call to pass column type

try:
    from columns import Password
except Exception:
    from .columns import Password

Base = declarative_base()

import simplejson as json

# Rama user level


class Level(Base):
    """
    Define level access to determine some command
    """

    __tablename__ = 'level'
    __table_args_ = {'schema': 'users'}

    ac_options = ["{:0{}b}".format(i,3) for i in range(8)]
    # ['000', '001', '010', '011', '100', '101', '110', '111']
    # First: same level all access
    # Second: same level own access
    # Third: under level all access

    id = Column(Integer, primary_key=True, autoincrement=True)
    value = Column(Integer, unique=True)
    group = Column(String(12), unique=True)
    description = Column(Text)
    modify_access = Column(String(3), unique=False)
    read_access = Column(String(3), unique=False)
    multiple_session = Column(Boolean, unique=False, default=False)

    # back tables:
    users = relationship('User', back_populates='level', order_by="User.id")
    commands = relationship('Command', back_populates='level', order_by="Command.id")

    @validates('value')
    def validate_value(self, key, value):
        assert isPositive(value)
        return value

    def __str__(self):
        return str(self.value) + " :" + self.group + " :" + self.description

    def __repr__(self):
        level = self.toJSON()
        return json.dumps(level)

    def toJSON(self):
        level = {'value': self.value,
                 'group': self.group,
                 'description': self.description,
                 'modify': self.modify_access,
                 'read': self.read_access,
                 'multiple_session': self.multiple_session}
        return level


    def show_ac_options(self):
        return self.ac_options

    @classmethod
    def show_ac_options(cls):
        return cls.ac_options

    def get_modify_ac(self):
        return self.modify_access

    def get_read_ac(self):
        return self.read_access

    def get_value(self):
        return self.value

    def has_multiple_session(self):
        return self.multiple_session


class User(Base):
    """
    Define user with some level access
    """

    __tablename__ = 'user'
    __table_args_ = {'schema': 'users'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(20), unique=True)
    password = Column(Password(rounds=13))
    level_id = Column(Integer, ForeignKey('level.id'))

    level = relationship("Level", back_populates="users")

    @validates('password')
    def _validate_password(self, key, password):
        return getattr(type(self), key).type.validator(password)

    def __str__(self):
        return self.name + " :" + str(self.level_id)

    def __repr__(self):
        user = self.toJSON()
        return json.dumps(user)

    def toJSON(self):
        return {'name': self.name, 'level_id': self.level_id}



# print 'Access granted'


class Command(Base):
    """
    Define what commands the users can do throw the server
    """
    __tablename__ = 'command'
    __table_args_ = {'schema': 'users'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(20))
    description = Column(Text)
    level_id = Column(Integer, ForeignKey('level.id'))

    level = relationship("Level", back_populates="commands")
   
    def __str__(self):
        return "%s:%s:%s" %(self.name, self.level_id, self.description)

    def __repr__(self):
        command = self.toJSON()
        return json.dumps(command)

    def toJSON(self):
        command = {'name': self.name,
                   'description': self.description,
                   'low_level_id': self.level_id}

        return command



