from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

import os

print("=======DIR===========")

from networktools.environment import get_env_variable

data_db=dict(
	user=get_env_variable("UM_USER"),
	passw=get_env_variable("UM_PASSW"),
	hostname=get_env_variable("UM_HOST"),
	port=get_env_variable("UM_PORT"),
	dbname=get_env_variable("UM_DBNAME"),
    path=os.path.dirname(os.path.realpath(__file__)))


class ManagerSession:
    """
    Simple Manager to Database Session

    Allows to select the kind of database and give the correct session to the system
    """

    def __init__(self, *args, **kwargs):
        self.kind = kwargs.get('kind', 'sqlite')
        if self.kind == 'postgresql':
            self.db_engine = 'postgresql://{user}:{passw}@{hostname}:{port}/{dbname}'.format(**kwargs)
            self.engine = create_engine(self.db_engine)
            self.connection = self.engine.connect()
            self.session = sessionmaker(bind=self.engine)()
        else:
            path = kwargs.get('path')
            print(path)
            eng_path = 'sqlite:///%s/server.db' % path
            self.engine = create_engine(eng_path)
            self.connection = self.engine.connect()
            Session = sessionmaker(bind=self.engine)
            self.session = Session()

    def get_session(self):
        """
        Obtain the database's session

        :returns: The session

        """
        return self.session

print(data_db)

csession = ManagerSession(**data_db)
session = csession.get_session()
engine = csession.engine
connection = csession.connection

print(csession, session, engine, connection)
