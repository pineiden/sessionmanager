from sqlalchemy import update
from sqlalchemy import Table, MetaData
from saltpepper.nosql.manager import RethinkDBPepper

try:
    from .db_session import session, engine, connection
except Exception:
    from db_session import session, engine, connection
try:
    from .models import Level, User, Command
except Exception:
    from models import Level, User, Command

import datetime

try:
    from .password import PasswordHash
except Exception:
    from password import PasswordHash

try:
    from .session_handle import SessionHandle
except Exception:
    from session_handle import SessionHandle

try:
    from .tokenmanager import TokenManager
except Exception:
    from tokenmanager import TokenManager

from passlib.hash import bcrypt, argon2
import asyncio
from networktools.library import my_random_string
from networktools.colorprint import gprint, bprint, rprint

from datetime import datetime, timezone
import iso8601
import simplejson as json


class SessionGNS(SessionHandle):
    """
    An specific SessionHandle instance who extends to database model in the GNS
    case, has level, user, command, alert, message and log handlers for generic
    cases.
    """
    def __init__(self, pwcomx=12, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(pwcomx, int), "Password complexity must be integer"
        if pwcomx < 12:
            pwcomx = 12
        self.pwcmx = pwcomx
        print("PWCMX")
        print(self.pwcmx)
        # to manage tokens
        self.token_manager = TokenManager()
        # manage user sessions
        self.user_sessions = {}
        self.active_users = {}

    def level(self, this_value,
              group,
              description,
              modify,
              read,
              multiple_session,
              *args, **kwargs):
        """
        Create new level instance

        :param this_value: a value asigned to this particular level
        :param group: a name for this group
        :param description: a descrption
        :returns: Level instance
        """
        print(this_value)
        print(group)
        print(description)
        trues = {'True', 'true', 'Verdadero',
                 'verdadero', 'Si', 'Yes', 'YES',
                 'S', 'Y', '1', 1}
        falses = {'False', 'false', 'Falso',
                  'falso', 'No', 'N', 'NO', '0', 0}
        ms = False
        if multiple_session in trues:
            ms = True
        elif multiple_session in falses:
            ms = False
        ac_options = Level.show_ac_options()
        if not modify in ac_options:
            modify = '000'
        if not read in ac_options:
            read = '010'
        level = Level(value=this_value,
                      group=group,
                      description=description,
                      modify_access=modify,
                      read_access=read,
                      multiple_session=ms)
        self.create_level(level)
        return level

    def create_level(self, level):
        """
        Create the level instance on database

        :param level:

        :returns: void()
        """
        self.session.add(level)
        self.session.commit()

    def update_level(self, instance, fields, values):
        """
        Modify some fields from level instance identified by cid

        :param level: a table name
        :param fields: list of field names
        :param values:  list of field values

        :returns: void()
        """
        t_name = 'level'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def delete_level(self, level):
        """
        Delete a level and flush database
        :param level: Level instance

        :returns: void()
        """
        self.session.delete(level)
        self.session.flush()

    def get_levels(self):
        """
        Get a query list of levels
        :return:
        """
        return self.get_list(Level)

    def get_level_id(self, group):
        """
        Return a level id from table

        :param group:
        :returns:
        """
        u = self.session.query(Level).filter_by(group=group).all()
        level_ = u[0]
        level_id = level_.id
        return level_id

    def get_level_by_id(self, pk):
        """
        Return a level instance defined by id=pk

        """
        u = self.session.query(Level).filter_by(id=pk).all()
        level = u[0]
        return level

    def has_multiple_session(self, level_id):
        level = self.get_level_by_id(level_id)
        if level:
            return level.has_multiple_session()
        else:
            return False

    # create, update, delete user
    async def user(self, name, password, level_id, *args, **kwargs):
        """
        Create a new User instance
        :param name: name string
        :param password: password string, inside the method create the hash
        :param pw_comx: password complexity, int value
        :param level_id:

        :returns: user instance
        """
        psalt = await self.generate_salt(name)
        salt = psalt[0].get('salt')
        print("====salt===")
        print(salt)
        new_pass = "%s%s" %(password, salt)
        passw_hash = PasswordHash.new(new_pass, self.pwcmx)
        print(passw_hash)
        print("======PW hash====")
        print(new_pass)
        print(str(passw_hash))
        levels = [level.value for level in self.get_levels()]
        print("Levels========================")
        print(level_id)
        print(levels)
        if type(level_id)==str and level_id.isdigit():
            level_id = int(level_id)
        if not level_id in levels:
            level_id = max(levels)
        user = User(name=name, password=passw_hash, level_id=level_id)
        await self.create_user(user)
        return user

    async def create_user(self, user):
        """
        Save the user instance in database

        :param user: user instance
        :returns: void()
        """
        self.session.add(user)
        self.session.commit()

    async def update_user(self, username, password, fields, values, *args):
        """
        Modify user fields from user instance in
        database:

        :param instance: level instance
        :param fields: field name
        :param values: field value
        :returns:
        """
        t_name = 'user'
        assert len(fields) == len(values), "Fields y Values must have same number of elements"
        instance = await self.get_user(username)
        user_columns = User.__table__.columns.keys()
        bprint("======USER COLUMNS=======")
        gprint(user_columns)
        if instance:
            for f in fields:
                if f in user_columns:
                    v = values[fields.index(f)]
                    bprint("New name -> %s" %v)
                    if not (f == 'password' or f == 'name'):
                        bprint("%s -> %s" %(f,v))
                        rprint(repr(instance))
                        if f=='level_id':
                            level =  self.get_level_by_id(v)
                            self.update_table(t_name, instance, f, level.id)
                            return True, "%s changed to %s" %(f,v)
                    if f == 'name':
                        #print("Instance :::>")
                        #print(instance)
                        # generate salt with new username
                        result, user = await self.verify_password(username, password)
                        if result:
                            self.update_table(t_name, instance, f, v)
                            rprint("The password is correct, changing name")
                            newsalt, msg = await self.generate_salt(v)
                            new_pass = "%s%s" %(password, newsalt.get('salt'))
                            bprint("New password with salt->")
                            gprint(new_pass)
                            passw_hash = PasswordHash.new(new_pass, self.pwcmx)
                            # update the field
                            # delete the salt asociated to old username
                            await self.delete_salt(username)
                            # get the instance of the user with new name
                            newinstance = await self.get_user(v)
                            print("New instance")
                            print(newinstance)
                            bprint(newinstance.password)
                            print("Passwordhash")
                            print(passw_hash.hash)
                            print(repr(passw_hash))
                            # save changes
                            self.update_table(t_name, newinstance, 'password', passw_hash.hash)
                            return v, True
                    if f == 'password':
                        return "You must use change_password function, despite", False
                else:
                    return False, "Column doesn't exist"
        else:
            return False, "Error with data"

    async def change_password(self, name, orig_pass, passw1, passw2):
        check, user = await self.verify_password(name, orig_pass)
        bprint("NEW PASSWORD %s=%s, check=%s" %(passw1, passw2, check))
        if passw1 == passw2 and check:
            #print("Instance :::>")
            #print(instance)
            name = name
            instance = await self.get_user(name)
            salt_result = await self.get_salt(name)
            rprint("SALT BEFORE")
            bprint(salt_result)
            gprint(instance.password)
            await self.delete_salt(name)
            salt, result = await self.generate_salt(name)
            new_pass = "%s%s" %(passw1, salt.get('salt'))
            #print("########La nueva password en server será########")
            #print(new_pass)
            passw_hash = PasswordHash.new(new_pass, self.pwcmx)
            self.update_table('user', instance, 'password', passw_hash.hash)
            instance = await self.get_user(name)
            rprint("SALT AFTER")
            bprint(salt)
            gprint(instance.password)
            return True, "Password Changed Correctly"
        else:
            return False, "New Passwords are not equal "

    async def verify_password(self, user_name, password, *args):
        """
        Check if password gived is ok

        :param user_name: string user name
        :param password: password as normal string
        :returns: Boolean, User Instance
        """
        #print("Verificando password")
        u = await self.get_user(user_name)
        v = False
        if u:
            salt_result = await self.get_salt(user_name)
            salt = salt_result[0].get('salt')
            pass_check = "%s%s" %(password, salt)
            print("====PASS CHECK===")
            print(pass_check)
            print("="*50)
            print(u)
            print(u.__dict__)
            o_pw = u.password.hash  # Password instance
            #print("======PW hash====")
            #print(o_pw)
            v = argon2.verify(pass_check, o_pw)
        return v, u

    async def login(self, username, password, id_origin, activate=True, *args):
        """
        TODO: Define a good criteria

        """

        result, user = await self.verify_password(username, password)

        allow_ms = False
        bprint("====ACTIVATING....=====")
        rprint(result)
        gprint(activate)
        if result:
            if user in self.user_sessions:
                bprint("User is in user_sessions")
                rprint("Activating a new user,---")
                allow_ms = self.user_sessions.get('allow_ms')
                if allow_ms:
                    if result and activate:
                        self.activate_user(username, id_origin)
                    elif not result and activate:
                        # if active user is empty
                        if not self.active_user:
                            self.activate_user(None)
                    elif result and not activate:
                        # If active_user is empty
                        if not self.active_user:
                            self.activate_user(None)
                    else:
                        self.activate_user(None)
                    self.user_sessions.get(
                        username).get('origins').append(id_origin)
                    return user, result, "Session added to this user"
                else:
                    return user, False, "More than one session isn't allowed"
            else:
                bprint("User is NOT in user_sessions")
                rprint("Activating a new user,---")
                multiple_session = self.has_multiple_session(user.level_id)
                self.user_sessions.update(
                    {
                        username: {
                            'allow_ms': multiple_session,
                            'origins': [id_origin]
                        }
                    }
                )
                return user, result, "Is the first session for this user"
        else:
            return user, result, "User doesn't exist or password is incorrect"


    async def activate_user(self, id_origin, username):
        token = self.token_manager.get_token(id_origin, username)
        # exists username and the token, so its logged
        result = False
        active_user = {}
        if username and token:
            active_user = {'username': username, 'token': token}
            result = True
        else:
            if not self.active_users:
                active_user = {'username': None, 'token': None}
        self.active_users.update({id_origin: active_user})
        bprint("======ACTIVE USER IS::::=========")
        rprint(active_user)
        return active_user, result

    async def logout(self, credentials, *args):
        print("Credentials to logout")
        print(credentials)
        if credentials.get('token'):
            active_user = credentials.get('args')[0]
            return active_user, True
        else:
            return None, None

    async def delete_user(self, user):
        """
        Delete a level and flush database

        :param level: Level instance

        :returns: void()
        """
        instance = await self.get_user(user)
        user = repr(instance)
        self.session.delete(instance)
        self.session.flush()
        return True, user

    async def get_users(self, *args):
        """
        Get users list

        :returns: query list
        """        
        return self.get_list(User)

    async def get_user(self, name, *args):
        user = self.session.query(User).filter_by(name=name).all()
        if user:
            #print(user[0])
            return user[0]
        else:
            return None

    async def get_ac(self, username, *args, **kwargs):
        user = await self.get_user(username)
        level_id = user.level_id
        level = self.get_level_by_id(level_id)
        modify = level.get_modify_ac()
        read = level.get_read_ac()
        return level, modify, read

    async def new_user(self, username, password, password2, level, *args):
        options = {'level': level}
        if password == password2:
            user = await self.user(username, password, level)
            if user:
                return True, repr(user)
            else:
                return False, repr(user)
        else:
            return False, False

    async def check_acl(self, orig_username, username, *args, **kwargs):
        """
        Define if the relation between two user can be posible:

        - modify
        - read

        """
        level, modify, read = await self.get_ac(orig_username)
        orig_level_value = level.get_value()
        exist_user = await self.get_user(username)
        bprint("Existe nuevo usuario?")
        rprint(repr(exist_user))
        can_mod = False
        can_read = False
        if exist_user:
            des_level, des_modify, des_read = await self.get_ac(username)
            level_value = des_level.get_value()
            if orig_level_value <= level_value:
                first = int(modify[0])
                second = int(modify[1])
                third = int(modify[2])
                if orig_level_value == level_value:
                    if not orig_username == username:
                        if first == 1:
                            #do
                            can_mod = True
                        else:
                            # not authorized
                            can_mod = False
                    else:
                        if second == 1:
                            #do
                            can_mod = True
                        else:
                            # not authorized
                            can_mod = False
                elif orig_level_value < level_value:
                        if third == 1:
                            #do
                            can_mod = True
                        else:
                            # not authorized
                            can_mod = False
                first = int(read[0])
                second = int(read[1])
                third = int(read[2])
                if orig_level_value == level_value:
                    if not orig_username == username:
                        if first == 1:
                            #do
                            can_read = True
                        else:
                            # not authorized
                            can_read = False
                    else:
                        if second == 1:
                            #do
                            can_read = True
                        else:
                            # not authorized
                            can_read = False
                elif orig_level_value < level_value:
                        if third == 1:
                            #do
                            can_read = True
                        else:
                            # not authorized
                            can_read = False
        else:
            # if new user doesn't exist
            level_value = kwargs.get('level', 1)
            print("=====LEVEL VALUE=====")
            print(level_value)
            if orig_level_value <= level_value:
                first = int(modify[0])
                second = int(modify[1])
                third = int(modify[2])
                if orig_level_value == level_value:
                    if not orig_username == username:
                        if first == 1:
                            #do
                            can_mod = True
                        else:
                            # not authorized
                            can_mod = False
                    else:
                        if second == 1:
                            #do
                            can_mod = True
                        else:
                            # not authorized
                            can_mod = False
                elif orig_level_value < level_value:
                        if third == 1:
                            #do
                            can_mod = True
                        else:
                            # not authorized
                            can_mod = False
                first = int(read[0])
                second = int(read[1])
                third = int(read[2])
                if orig_level_value == level_value:
                    if not orig_username == username:
                        if first == 1:
                            #do
                            can_read = True
                        else:
                            # not authorized
                            can_read = False
                    else:
                        if second == 1:
                            #do
                            can_read = True
                        else:
                            # not authorized
                            can_read = False
                elif orig_level_value < level_value:
                        if third == 1:
                            #do
                            can_read = True
                        else:
                            # not authorized
                            can_read = False
        return can_mod, can_read

    async def filter(self, credentials):
        """
        Wrapper the principal actions to generalize the use

        :param credentials: A dictionary with all the necesary information {action, args, token}

        :returns: result, credentials (modified if the given case)
        """
        #print("====FILTERING===")
        #print([bprint("key %s --> value %s" %(key, value)) for key, value in credentials.items()])
        action = credentials.get('action', None)
        args = credentials.get('args', [])
        # TOKEN VALUE IS A JSON DICT WITH {value, username origin} keys
        # This exists after login
        token_json = credentials.get('token', None)
        token, username, origin = None, None, None
        #bprint("======TOKEN INPUT=====>>>>>>>>>>")
        #rprint(token_json)
        if isinstance(token_json, dict) and token_json:
            token = token_json.get('value') 
            username = token_json.get('username')
            origin = token_json.get('origin')
        actions = {
            'login': self.login,
            'logout': self.logout,
            'list_users': self.get_users,
            'set_user': self.activate_user,
            'get_info': self.get_info,
            'get_user': self.get_user,
            'change_password': self.change_password,
            'delete_user': self.delete_user,
            'create_user': self.new_user,
            'update_user': self.update_user,
            'send_msg': self.check_token,
            'renew_token': self.renew_token,
            'get_groups': self.get_all_levels
        }
        modify_user = {'create_user',
                       'delete_user',
                       'change_password'}
        if action in actions:
            if token and not action == 'login' and \
               not action == 'logout':
                # obtener usuario a partir de token
                orig_username = username
                result = None
                # what to do with credentials --->
                # update the values
                if action == 'get_info':
                    username = args[0]
                    can_mod, can_read = await self.check_acl(orig_username,
                                                             username)
                    if can_mod:
                        result = await actions.get(action)(*args)
                elif action == 'get_groups':
                    result = await actions.get(action)(*args)
                elif action == 'list_users':
                    # hay un mejor filtro, que es
                    # haciendo la consulta ya filtrada
                    # mientras tanto, es suficiente
                    users = await actions.get(action)(*args) # list of users
                    #rprint("List users command->")
                    #gprint(orig_username)
                    #bprint(users)
                    check = []
                    for user in users:
                        rev = await self.check_acl(orig_username, user.name)
                        check.append(rev)
                    #rprint("Checking")
                    #bprint(check)
                    result = []
                    for i, user in enumerate(users):
                        if check[i][1]:
                            result.append(json.loads(repr(user)))
                    #gprint("Filtered:::")
                    #[print(user) for user in result]
                elif action == 'get_user':
                    username = args[0]
                    can_mod, can_read = await self.check_acl(orig_username,
                                                             username)
                    if can_read:
                        # here result is user object instance
                        result = await actions.get(action)(*args)
                        result = result.toJSON()
                elif action == 'set_user':
                    coro = actions.get(action)
                    #bprint("RUNNING   ->>>>")
                    #rprint(coro)
                    result, is_active = await coro(*args)
                    #rprint("Post set user")
                    #bprint(result)
                    #bprint(credentials)
                    token = result.get('token')
                    result.update({'token':token.toJSON()})
                    return result, credentials
                elif action in modify_user:
                    username = args[0]
                    level_id = None
                    #bprint("ARGS_____>")
                    #rprint(args)
                    if len(args) > 3:
                        if type(args[3]) == str:
                            if args[3].isdigit():
                                level_id = int(args[3])
                    if action == 'create_user':
                        level_id = 1
                    can_mod, can_read = await self.check_acl(orig_username,
                                                             username,
                                                             level=level_id)
                    if can_mod:
                        result = await actions.get(action)(*args)
                    else:
                        result = False, "Cannot %s user" %action
                elif action == 'send_msg':
                    args.insert(0, token_json)
                    result = await actions.get(action)(*args)
                    available = result[0]
                    token = None
                    if available:
                        token = result[1]
                        if token:
                            credentials.update({'token': token})
                elif action == 'renew_token':
                    result = await actions.get(action)(*args)
                    result = result()
                    token = result
                    credentials.update({'token': token})
                elif action == 'update_user':
                    #bprint("ARGS_____>")
                    #rprint(args)
                    origin = args[-1]
                    username = args[3][0]
                    can_mod, can_read = await self.check_acl(orig_username,
                                                             username)
                    data = self.token_manager.get_token(origin, username)
                    if can_mod:
                        #bprint("=====ACTION==========")
                        result = await actions.get(action)(*args)
                        #rprint("Result")
                        #gprint(result)
                        if data:
                            # si el usuario que se modifica está
                            # en la lista de tokens activo
                            # se regenera el token
                            bprint("===DATA OK====")
                            await self.token_manager.generate_token(origin,
                                                              username)
                            token = self.token_manager.get_token(origin,
                                                                 username)
                            rprint(token)
                            credentials.update({'token': token()})
                    # update credentials?
                #bprint("ENVIANDO---->")
                #rprint(result)
                #gprint(credentials)
                return result, credentials
            elif action == 'login':
                # what to do with login?
                # get the token
                # verify password
                # args: (name, password)
                # return boolean, user
                #print("LOGIN")
                #print(args)
                id_origin = args[-2]
                activate = args[-1]
                user, result, msg = await actions.get(action)(*args)
                is_ok = result
                token = None
                #bprint("CHECK LOGIN...generating token if TRUE")
                #rprint(result)
                #bprint("=====ACTION==========")
                if is_ok:
                    await self.token_manager.generate_token(id_origin,
                                                            user.name)
                    token = self.token_manager.get_token(id_origin,
                                                         user.name)
                    #bprint("===DATA OK====")
                    #rprint(token)
                    #gprint(token.toJSON())
                    credentials.update({'token': token()})
                else:
                    #bprint("Is not ok")
                    credentials.update({'token': {'value': None}, 'msg': msg})
                is_active = False
                if activate:
                    #gprint("===============")
                    #bprint("Activate is TRUE")
                    #rprint(id_origin)
                    #gprint(user)
                    activation, is_active = await self.activate_user(
                        id_origin, user.name)
                    #rprint("Activation %s -> is active %s" %(activation, is_active))
                credentials.update({'is_active': is_active})
                return user.toJSON(), credentials
            elif action == 'get_info':
                is_ok, result = await actions.get(action)(*args)
                credentials.update({'info': result})
                #print("RESULTS OF LOGIN::::::")
                #print(credentials)
                #print(user)
                return is_ok, credentials
            elif action == 'logout':
                id_origin = args[-1]
                #gprint("INPUTS")
                #bprint(credentials)
                #rprint(args)
                # q is True if logged
                active_user, q = await actions.get(action)(credentials, *args)
                if q:
                    print(id_origin, active_user)
                    token = self.token_manager.get_token(id_origin,
                                                         active_user)
                    #bprint("====TOKENSSS===")
                    #rprint(token)
                    #gprint(type(token))
                    if token:
                        self.token_manager.delete_token(token)
                    else:
                        print("Token doesn't exists")
                    credentials.update({'token':{}})
                    return active_user, credentials
                else:
                    return args[-2], credentials
            else:
                return "You must LOGIN first", credentials
        else:
            return None, credentials

    async def check_token(self, token, *args, **kwargs):
        """
        Check if the token is active with the user, if the time is over
        generate a new token to this session
        """
        return self.token_manager.check_token(token)


    async def renew_token(self, id_origin, name, *args):
        """
        Check if the token is active with the user, if the time is over
        generate a new token to this session
        """
        token = await self.token_manager.renew_token(id_origin, name)
        return token


    async def generate_token(self, id_origin, name, *args):
        """
        Generate a new token, add this to the main set and manage
        on the dict tokens_info
        """
        await self.generate_token(id_origin, name)
        return self.token_mnanager.get_token(id_origin, name) 

    def delete_token(self, origin, name, *args, **kwargs):
        """
        Delete token from main set
        """
        self.token_manager.delete_token(origin, name)

    async def get_info(self, name, *args):
        user = await self.get_user(name)
        level_id = user.level_id
        level = self.get_level_by_id(level_id)
        commands = self.info_commands(level.id)
        return True, {'user': json.loads(repr(user)),
                'level':json.loads(repr(level)),
                'commands': json.loads(repr(commands))}

    def get_max_level(self):
        levels = self.get_levels()
        values = [level.value for level in levels]
        max_level = max(values)
        return max_level

    async def get_all_levels(self):
        levels = [repr(level) for level in self.get_levels()]
        return levels

    def info_commands(self, level):
        max_level = self.get_max_level()
        level_info_list = []
        if type(level) == int:
            for lev in range(level, max_level+1):
                level_info = self.get_level_by_id(lev)
                level_info_list.append(level_info)
        elif type(level) == str:
            if level.isdigit():
                for lev in range(int(level), max_level+1):
                    level_info = self.get_level_by_id(lev)
                    level_info_list.append(level_info)
        else:
            level_info = self.get_level_by_id(1)
        print("Level info")
        print(level_info_list)
        info_commands = []
        for level_info in level_info_list:
            [info_commands.append(command) for command in level_info.commands]
        return info_commands

    # create, delete command
    def command(self, command, description, level):
        """
        Create a new command instance

        :param name: name string
        :param command: command string
        :param description: description
        :param level_id: level int assigned
        :return: the Command instance
        """
        print("Level number:", level)
        level_id = self.session.query(Level).filter_by(value=level).first()
        print("Level id:", level_id.id)
        cmd = Command(
            name=command,
            description=description,
            level_id=level_id.id)
        self.create_command(cmd)
        return cmd

    def create_command(self, cmd):
        """
        Commit the command on the database

        :param cmd: Command instance
        :return: void()
        """
        self.session.add(cmd)
        self.session.commit()

    def update_command(self, instance, fields, values):
        """
        Change instace's fields from a command

        :param instance:
        :param fields:
        :param values:
        :return:
        """
        t_name = 'user'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def get_commands(self):
        """
        Get all list of commands
        :return: list of commands
        """
        return self.get_list(Command)

        # create, update, delete alert
