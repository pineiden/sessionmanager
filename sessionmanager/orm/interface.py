from sessionmanager.orm.db_users import SessionGNS
from networktools.colorprint import bprint, gprint, rprint
from networktools.docmethod import DocMethod
import time
import getpass
import inspect as ins


class SessionInterface:
    """
    A class to manage and interact with the Session Manager module.
    Allows a limited amount of actions and use a kind of polimorphism
    Also you can get information of all methods availables and add more if you need

    """
    action_list = {}
    reserved_actions = {
            'login',
            'logged',
            'logout',
            'list_users',
            'get_info',
            'change_password',
            'delete_user',
            'create_user',
            'update_user',
            'get_user',
            'send_msg',
            'renew_token',
            'set_user',
            'get_groups',
    }

    def __init__(self, *args, **kwargs):
        self.session = SessionGNS()
        actions = {
            'login': self.login,
            'logged': self.logged,
            'logout': self.logout,
            'list_users': self.list_users,
            'get_info': self.get_info,
            'get_user': self.get_user,
            'change_password': self.change_password,
            'delete_user': self.delete_user,
            'create_user': self.create_user,
            'update_user': self.update_user,
            'renew_token': self.renew_token,
            'set_user': self.set_user,
            'get_groups': self.get_groups,
            'get_actions_docs': self.get_actions_docs,
            'send_msg': self.send_msg,
        }
        self.action_list.update(actions)
        self.tokens = {}
        self.active_user = None

    async def do_action(self, action, *args, **kwargs):
        """
        Generic method to call an action with args

        :param action: The action's name
        :param args: The list of args
        :param kwargs: The dictionary of args with keyword
        :returns: The action result or False
        """
        rprint("===Les argumentos======")
        bprint(args)
        manage_session = {'logged', 'set_user'}
        if action in self.action_list:
            if not action in manage_session:
                return await self.action_list.get(action)(self.session,
                                                          *args,
                                                          **kwargs)
            else:
                rprint("===Les new argumentos======")
                bprint(args)
                return await self.action_list.get(action)(self.session,
                                                          *args,
                                                          **kwargs)

        else:
            return False, {}

    def get_actions_methods(self, *args, **kwargs):
        """
        Get the list of available methods for different actions on the interface

        :returns: list with DocMethods instances
        """
        methods = [self.action_list.get(name) for name in self.action_list]
        return list(map(DocMethod, methods))

    async def get_actions_docs(self, *args, **kwargs):
        """
        Get the information list of the methods availables for different actions on the interface

        :returns: list with the methods description in dictionary {name, doc, args_names}
        """
        doc_methods = self.get_actions_methods()
        list_docs = [method.get_all() for method in doc_methods]
        bprint("===ACTIONS DOCS===")
        rprint(list_docs)
        return list_docs, {}

    def get_actions(self, *args):
        """
        Return the dictionary with the actions availables

        :returns: the dictionary with the methods
        """
        return self.action_list

    @classmethod
    def add_method(cls, name, method, *args):
        """
        Add a new method to action_list dictionary

        :param name: The name of the method
        :type name: str
        :method: Function or method object
        :type method: function or method
        """
        if not name or not type(name) == str:
            return False, "name argument must be string"
        elif not name in cls.reserved_actions :
            if ins.ismethod(method) or ins.isfunction(method):
                cls.action_list.update({name: method})
                return True, 0
            else:
                return False, "method argument must be function or method"
        else:
            return False, "name %s is a reserved action" %name


    @classmethod
    def del_method(cls, name, *args):
        """
        Delete a method from the available list

        :returns: if method's name is in action_list, delete that, anyelse False

        """
        if not name or not type(name) == str:
            return False, "name argument must be string"
        elif  not name in cls.reserved_actions:
            if name in cls.action_list:
                return cls.action_list.pop(name)
            else:
                return False

    async def login(self, session,
                    username,
                    password,
                    id_origin,
                    activate,
                    *args, **kwargs):
        """
        Register the session of a user, using username and password

        :param session: the session instance to communicate with database
        :param username: string, the name of the user
        :param password: the password of the user

        :returns: the result of session.filter, a tuple 
        """
        print("Doing login")
        rprint("USERNAME---->%s" %username)
        bprint("PASS ----->%s" %password)
        username = kwargs.get('username', username)
        password = kwargs.get('password', password)
        credentials = {
            'action': 'login',
            'args': [username, password, id_origin, activate]
        }
        rprint("Credentials LOGIN")
        bprint(credentials)
        result = await session.filter(credentials)
        gprint("Result login")
        rprint(result)
        return result

    async def get_info(self, session, token, username, *args, **kwargs):
        """
        Get information related with some specific user

        :param session: the session instance to communicate with database
        :param username: string, the name of the user
        :param token: string, is the token generated for this session

        :returns: The user's information
        """
        credentials = {
            'action': 'get_info',
            'token': token,
            'args': [username]
        }
        return await session.filter(credentials)

    async def get_groups(self, session, token, *args, **kwargs):
        """
        Is the method that returns the list of available groups on the system

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session

        :returns: The groups's information

        """
        credentials = {
            'action': 'get_groups',
            'token': token,
            'args': []
        }
        return await session.filter(credentials)

    async def change_password(self,
                        session,
                        token,
                        username,
                        passw0,
                        newpass1,
                        newpass2, *args, **kwargs):
        """
        Change the password from some specific user.

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session
        :param username: string, the username
        :param passw0: string, original password
        :param newpass1: string, new password
        :param password2: string, same as newpass1

        :returns: The groups's information

        """
        credentials = {
            'action':'change_password',
            'token': token,
            'args': [username, passw0, newpass1, newpass2,]
        }
        return await session.filter(credentials)

    async def delete_user(self, session, token, username, *args):
        """
        Delete some specific user

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session
        :param username: string, the username

        :returns: The action's verification 
        """
        ## check authorization
        ## maybe ask token, then check user group
        credentials = {
            'action': 'delete_user',
            'token': token,
            'args': [username]
        }
        return await session.filter(credentials)

    async def create_user(self, session, token, username,
                          password,
                          password2,
                          level_group, *args, **kwargs):
        """
        Create a new user

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session
        :param username: string, the username
        :param newpass1: string, new password
        :param password2: string, same as newpass1

        :returns: The action's verification 
        """
        credentials = {
            'action': 'create_user',
            'token': token,
            'args': [username, password, password2, level_group]
        }
        return await session.filter(credentials)

    async def update_user(self, session,
                          token,
                          username,
                          password,
                          fieldname,
                          value,
                          id_origin,
                          *args, **kwargs):
        """
        Update some user's information

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session
        :param username: string, the username
        :param fieldname: string, field name from user atribute
        :param value: string, value for the atribute

        :returns: The update's action 
        """
        credentials = {
            'action': 'update_user',
            'token': token,
            'args': [username, password, fieldname, value, id_origin]
        }
        return await session.filter(credentials)

    # define functions related to selection, and ask about the args

    async def list_users(self, session, token, *args, **kwargs):
        """
        Realize a query to obtain the users's list

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session

        :returns: The list of users
        """
        credentials = {
            'action': 'list_users',
            'token': token,
            'args': []
        }
        return await session.filter(credentials)

    async def get_user(self, session, token, username, *args, **kwargs):
        """
        Realize a query to obtain the user's information

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session
        :param username: string, the username

        :returns: The list of users
        """
        credentials = {
            'action': 'get_user',
            'token': token,
            'args': [username]
        }
        return await session.filter(credentials)

    async def send_msg(self, session, token, msg, id_origin, *args, **kwargs):
        """
        Check the token and aprove the msg to be sended

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session
        :param msg: string, the message

        :returns: The aprovation or not of the message
        """
        credentials = {
            'action': 'send_msg',
            'token': token, 
            'args': [msg, id_origin]
        }
        return await session.filter(credentials)

    async def renew_token(self, session, token, username, id_origin, *args, **kwargs):
        """
        Create a new token

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session
        :param username: string, the username

        :returns: The aprovation or not of the message
        """
        credentials = {
            'action': 'renew_token',
            'token': token,
            'args': [id_origin, username]
        }
        return await session.filter(credentials)

    async def logged(self, session, token, *args, **kwargs):
        """
        Get the logged users with this session.
        Allows multiple users

        :param tokens: list, the list of tokens

        """
        tokens = self.session.token_manager.get_list()
        #rprint("Tokens info")
        #gprint(tokens)
        logged_list = [(token.user, token.value) for token in tokens]
        #[bprint("%s -> %s" % (token.user, token.value)) for token in tokens]
        credentials = {
            'action': 'logged',
            'token': token,
            'args': []
        }
        #rprint(logged_list)
        credentials.update({'answer':logged_list})
        #gprint("With logged list")
        #bprint(credentials)
        if logged_list:
            return True, credentials
        else:
            return False, credentials

    async def logout(self, session, token, username, origin, *args, **kwargs):
        """
        Create a new token

        :param session: the session instance to communicate with database
        :param token: string, is the token generated for this session
        :param username: string, the username
        :param tokens: list, the list of tokens

        :returns: The aprovation or not of the message
        """
        #print("LOGOUT")
        #bprint(self.session.token_manager.get_list())
        #rprint(token)
        #result, token_ob = self.session.token_manager.check_token(token)
        #bprint("TOKEN OBJECT ON DELETE")
        #rprint(token_ob)
        #origin = token_ob.origin
        #self.session.token_manager.delete_token(token_ob)
        credentials = {
            'action': 'logout',
            'token': token,
            'args': [username, origin]
        }
        return await session.filter(credentials)

    async def set_user(self, session, token, username, origin, *args, **kwargs):
        """
        Check if username is in tokens, returns username

        """
        ## check if token is active with user:
        #gprint("="*40)
        #print("SET USER")
        #gprint("="*40)
        credentials = {
            'action': 'set_user',
            'token': token,
            'args': [origin, username]
        }
        self.dictprint(bprint, credentials)
        return await session.filter(credentials)


    def listprint(self, callback, contentlist):
        """
        Print the list of elements in a orderer way

        """
        [callback(elem) for elem in contentlist]

    def dictprint(self, callback, contentlist):
        """
        Print the list of elements in a orderer way

        """
        [callback("%s --> %s" %(key, value)) for key, value in contentlist.items()]



    async def term_input(self, action, *args, **kwargs):
        """
        Create an interactiva terminal that take the action and execute something

        :param action: the action's name

        :returns: something

        """
        origin = kwargs.get('origin', '000')
        action_list = self.get_actions()
        session = self.session
        tokens = self.tokens
        active_user = self.active_user
        args = [origin]
        if action in action_list:
            if action == 'login':
                username = input("Username:\n")
                password = getpass.getpass("Password:\n")
                rprint("Parameters")
                bprint([action, username, password, args])
                rprint(action_list)
                result, credentials = await action_list.get(action)(
                    session, username, password, *args)
                bprint("===RESULT====")
                rprint(result)
                rprint(credentials)
                if credentials.get('token'):
                    active_user = username
                    tokens.update({username: credentials.get('token')})
                    bprint("TOken list")
                    rprint(tokens)
                    gprint(self.tokens)
            elif action == 'logged':
                print("=======TOKENS======")
                bprint(tokens)
                result, credentials = await action_list.get(action)(
                    session, tokens, tokens.get(
                        active_user).get('value'), *args)
            elif action == 'list_users':
                token = tokens.get(active_user)
                result, credentials = await action_list.get(action)(
                    session, token, *args)
                bprint("<===== Users ====>")
                self.listprint(rprint, result)
                rprint(credentials)
            elif action == 'logout':
                token = tokens.get(active_user)
                bprint("Pre LOGOUT")
                action_list.get('logged')(tokens)
                username = input("Username:\n")
                if username in tokens:
                    result, credentials = await action_list.get(action)(
                        session,
                        token,
                        username,
                        token,
                        *args)
                    if username == active_user:
                        active_user = None
                        gprint("Remember to set active user")
                bprint("Post LOGOUT")
                action_list.get('logged')(tokens)
            elif action == 'set_user':
                token = tokens.get(active_user)
                username = input("Username: \n")
                bprint("PARAMS SET_USER")
                rprint(token)
                rprint(username)
                gprint(args)
                rprint(action_list.get(action))
                coro = action_list.get(action)
                bprint(coro)
                active_user, is_active, result = await coro(
                    session,
                    token,
                    username,
                    *args)
                rprint(active_user)
                gprint(result)
                bprint("Active User is %s now" % repr(active_user))
            elif action == 'get_info':
                username = input("Username:\n")
                token = tokens.get(active_user)
                result, credentials = await action_list.get(action)(
                    session,
                    token,
                    username,
                    *args)
                bprint("===RESULT====")
                bprint("Level:>:>")
                rprint(result.get('level'))
                bprint("Commands:>:>")
                self.listprint(rprint, result.get('commands'))
                rprint(credentials)
            elif action == 'change_password':
                token = tokens.get(active_user)
                username = input("Username:\n")
                password0 = getpass.getpass("Original Password:\n")
                password1 = getpass.getpass("New Password:\n")
                password2 = getpass.getpass("Again, new Password:\n")
                result, credentials = await action_list.get(action)(
                    session,
                    token,
                    username,
                    password0,
                    password1,
                    password2,
                    *args)
                bprint("===RESULT====")
                rprint(result)
                rprint(credentials)
            elif action == 'create_user':
                token = tokens.get(active_user)
                username = input("Username:\n")
                level_group = input("Level group:\n")
                password0 = getpass.getpass("Original Password:\n")
                password1 = getpass.getpass("Again, original Password:\n")
                if password0==password1:
                    result, credentials = await action_list.get(action)(
                        session,
                        token,
                        username,
                        password0,
                        password1,
                        int(level_group),
                        *args)
                    bprint("===RESULT CREATE USER====")
                    rprint(result)
                    rprint(credentials)
                if not result:
                    print("Las password enviada no coinciden")
            elif action == 'delete_user':
                token = tokens.get(active_user)
                username = input("Username to delete:\n")
                result, credentials = await action_list.get(action)(
                    session,
                    token,
                    username,
                    *args)
                bprint("===RESULT====")
                rprint(result)
            elif action == 'update_user':
                token = tokens.get(active_user)
                username = input("Username to modify:\n")
                password = getpass.getpass("Username's Password:\n")
                fieldname = input("Input fieldname:\n")
                if fieldname == 'name':
                    value = input("Insert newname:\n")
                    result, credentials = await action_list.get(action)(
                        session,
                        token,
                        username,
                        password,
                        [fieldname],
                        [value],
                        *args)
                elif fieldname == 'password':
                    password1 = getpass.getpass("New Password:\n")
                    password2 = getpass.getpass("Again, new Password:\n")
                    result, credentials = await action_list.get('change_password')(
                        session,
                        token,
                        username,
                        password,
                        password1,
                        password2,
                        *args)
                elif fieldname == 'level':
                    value = input("Insert the new level:\n")
                    result, credentials = await action_list.get(action)(
                        session,
                        token,
                        username,
                        password,
                        [fieldname],
                        [value],
                        *args)
            elif action == 'get_user':
                token = tokens.get(active_user)
                username = input("Obtain info from this username:\n")
                result, credentials = await action_list.get(action)(
                    session,
                    token,
                    username,)
                bprint("===RESULT====")
                rprint(repr(result))
            elif action == 'send_msg':
                token = tokens.get(active_user)
                mensaje = input("Enviar mensaje:\n")
                msg = {'msg':mensaje}
                result, credentials = await action_list.get(action)(
                    session,
                    token,
                    active_user,
                    msg,
                    *args)
                bprint("===RESULT====")
                rprint(repr(result))
            elif action == 'renew_token':
                token = tokens.get(active_user)
                result, credentials = await action_list.get(action)(
                    session,
                    token,
                    active_user,
                    *args)
                bprint("===RESULT====")
                rprint(repr(result))
            elif action == 'get_actions_doc':
                credentials = {}
                result = await action_list.get(action)()
                [[print("%s -> %s" %(elem, action.get(elem))) for elem in action] for action in result]
            else:
                bprint("Action %s is not defined yet" %action)


        else:
            bprint("Unknown action, use the list")

        self.active_user = active_user
