from passlib.hash import bcrypt, argon2
from sqlalchemy.ext.mutable import Mutable


def get_rounds(hashv, crypt):
	"""
	This functions check some attributes from the hash related by different method

    :returns: the number of rounds
	"""
	crypto_dict={bcrypt:0, argon2:1}
	rounds = 30
	if crypto_dict.get(crypt)==0:
		rounds = hashv.split('$')[2]
	elif crypto_dict.get(crypt)==1:
		v_rounds = hashv.split('$')[3]
		q_rounds = v_rounds.split(",")[1]
		rounds = q_rounds.split("=")[1]
	return int(rounds)


class PasswordHash(Mutable):
    """
    Create a password hash with bcrypt or argon2
    """

    crypt_factory = argon2

    def __init__(self,  hash_, rounds=12, crypt_factory=argon2):
		#amount of $ by method		
        crypto_dict_count = {bcrypt:3, argon2:5}
        lens_disc = {bcrypt:False, argon2:True}
        crypto_dict_len = {bcrypt:60, argon2:75}
        crypto_dict_limit = {bcrypt:15, argon2:500}
        assert crypt_factory in crypto_dict_count,"Insert a valid encrypt function"
        self.crypt_factory = crypt_factory
        # catch the first
        self.rounds = get_rounds(hash_, self.crypt_factory)
        # rounds related to limir
        if rounds<=crypto_dict_limit.get(self.crypt_factory):
            self.desired_rounds = rounds or self.rounds
        else:
            self.desired_rounds = self.rounds
        self.lround = len(str(self.rounds))
        # ensure a valid crypt_factory method
        if not crypt_factory in crypto_dict_len:
            crypt_factory = argon2	
        lens = {}
        if lens_disc.get(crypt_factory):
            len_hash = len(hash_)-self.lround
        else:
            len_hash = len(hash_)
        print(len_hash)
        print(crypto_dict_len.get(self.crypt_factory))
        assert len_hash == crypto_dict_len.get(self.crypt_factory), 'hash should be 60 schars'
        amount = crypto_dict_count.get(crypt_factory)
        assert hash_.count('$')==amount, 'hash should have %s x "$"' % amount
        self.hash = str(hash_)

    def __eq__(self, candidate):
        """Hashes the candidate string and compares it to the stored hash."""
        if isinstance(candidate, str):
            if self.hash == self.crypt_factory.encrypt(candidate, rounds=self.rounds):
                if self.rounds < self.desired_rounds:
                    self._rehash(candidate)
                return True
        return False

    def __repr__(self):
        """Simple object representation."""
        return '<{}>'.format(type(self).__name__)

    def __str__(self):
        return self.hash

    @classmethod
    def coerce(cls, key, value):
        """Ensure that loaded values are PasswordHashes.
		given value, this is coerced to be converted y a PasswordHash type
		method inherited from Mutable
		Si no es del tipo, entonces retora un raise error 
		"""
        if isinstance(value, PasswordHash):
            return value
        return super(PasswordHash, cls).coerce(key, value)

    @classmethod
    def new(cls, password, rounds):
        """Creates a PasswordHash from the given password.
        :returns: the new hash for this password
        """
        assert isinstance(password, str)
        return cls(cls.crypt_factory.using(rounds=rounds).encrypt(password))

    @classmethod
    def set_crypt(cls, crypt):
        """Set a PasswordHash from the given password."""
        cls.crypt_factory = crypt

    @staticmethod
    def _new(password, rounds):
        """Returns a new bcrypt hash for the given password and rounds."""
        return PasswordHash.crypt_factory.using(rounds=rounds).encrypt(password)

    def _rehash(self, password):
        """Recreates the internal hash and marks the object as changed."""
        self.hash = self._new(password, rounds=self.desired_rounds)
        self.rounds = self.desired_rounds
        self.changed()
