from passlib.hash import argon2

#https://passlib.readthedocs.io/en/stable/lib/passlib.hash.argon2.html#passlib.hash.argon2
#$argon2X$v=V$m=M,t=T,p=P$salt$digest

password="<Esta es mi password>"

print("Esta es mi password en ASCII %s" %password)
argon2_hash = argon2.hash(password)
print("Esta es mi password en ASCII %s" %argon2_hash)
print("Rounds")
print(int(argon2_hash.split('$')[2]))
