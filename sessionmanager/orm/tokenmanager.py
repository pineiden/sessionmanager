from networktools.library import my_random_string
from datetime import datetime, timezone
import asyncio
from networktools.colorprint import gprint, bprint, rprint

try:
    from .session_token import Token
except Exception:
    from session_token import Token

class TokenManager:
    # lista limpia de tokens
    tokens = set()
    # diccionario name -> token
    tokens_user = dict()
    # path origin-username
    tokens_path = dict()

    def __init__(self, *args, **kwargs):
        # set size of token for all tokens
        self.uin = kwargs.get('uin', 64)

    def set_uin(self, uin):
        self.uin = uin

    def get_uin(self):
        return self.uin

    async def renew_token(self, id_origin: str, name: str, *args, **kwargs):
        """
        Check if the token is active with the user, if the time is over
        generate a new token to this session
        """
        #bprint("====ORIGIN-NAME====")
        #rprint("%s   %s" %(id_origin, name))
        return await self.generate_token(id_origin, name)

    async def generate_token(self, 
                             id_origin: str,
                             name: str,
                             *args,
                             **kwargs):
        token = None
        new_token = True
        token = self.get_token(id_origin, name)
        # check if exist name
        if token:
            token.generate()
            new_token = False
        else:
            kwargs = {
                'uin': self.uin,
                'name': name,
                'origin': id_origin
            }
            token = Token(**kwargs)
            #bprint("===NEW TOKEN GENERATED===")
            while token in self.tokens:
                token.generate()
            new_token = token
        token.update_history()
        if new_token:
            self.add2set(token)
            self.add2path(token, id_origin, name)
            self.add2user(token, id_origin, name)
        #bprint("TOKENS USER")
        #rprint(self.tokens_user)
        return token

    def get_token(self, origin, name):
        token = None
        #rprint("Set of tokens")
        #bprint(self.tokens)
        for token in self.tokens:
            #bprint("Token -----> %s" %token.toJSON())
            #rprint("%s --> %s" %(origin, name))
            if origin == token.origin and name == token.user:
                #gprint("IS TOKEN IN SET")
                return token
        return None

    def add2set(self, token: Token):
        self.tokens.add(token)

    def add2path(self, token, origin, name):
        self.tokens_path.update({token: (origin, name)})

    def add2user(self, token: Token,
                 origin: str,
                 name: str,):
        # if exist origin-user
        info_origin = self.tokens_user.get(origin, None)
        if info_origin:
            info_user = info_origin.get(name, None)
            if not info_user:
                info = {name: token}
                info_origin.update(info)
        else:
            info = {origin: {name: token}}
            self.tokens_user.update(info)

    def is_logged(self, origin, name):
        token_set = self.tokens_user.get(origin, None)
        token = None
        if token_set:
            token = token_set.get(name, None)
        if token in self.tokens:
            return True
        else:
            return False

    def delete_token(self, token):
        """
        Delete token from main set
        When do logout
        """
        #print("=======On delete token=======")
        #bprint(token)
        #rprint(type(token))
        if token in self.tokens:
            #rprint("Token in tokens")
            self.tokens.remove(token)
        else:
            bprint("Token not exist on tokens")

    def get_list(self):
        return list(self.tokens)

    def check_token(self, token_dict: dict):
        first = False
        token_find = None
        #rprint("===CHECK TOCKEN===")
        #bprint(token_dict)
        #rprint(type(token_dict))
        for token in self.tokens:
            #rprint(token)
            #bprint(type(token))
            if token_dict.get("value") == token.value:
                first = True
                token_find = token
                dt_gen = token_find.dategen
                dt_now = datetime.now(timezone.utc)
                diff_min = dt_now - dt_gen
                if diff_min.seconds/60 >= 30:
                    user = token_find.user
                    origin = token_find.origin
                    self.generate_token(origin, user)
                return first, token_find
        return first, token_find
