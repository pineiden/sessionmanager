from networktools.library import my_random_string
from datetime import datetime, timezone
import simplejson as json


class Token:
    _value = None
    _history = list()

    def __init__(self, *args, **kwargs):
        self._uin = kwargs.get('uin', 64)
        self.generate()
        self.update_history()
        self._user = kwargs.get('name', None)
        self._origin = kwargs.get('origin', None)

    def generate(self):
        uin = self._uin
        token = my_random_string(uin)
        self._value = token
        self._date_gen = datetime.now(timezone.utc)

    @property
    def dategen(self):
        return self._date_gen

    @property
    def history(self):
        return self._history

    @property
    def value(self):
        return self._value

    @property
    def uin(self):
        return self._uin

    @property
    def user(self):
        return self._user

    @property
    def origin(self):
        return self._origin

    def update_history(self):
        self._history.append((self._date_gen, self.value))

    def __hash__(self):
        return hash((self._user, self._origin))

    def __str__(self):
        return self._value

    def __repr__(self):
        return self._value

    def __call__(self, *args, **kwargs):
        return self.toJSON()

    def __eq__(self, other_token):
        return self.value == other_token.value 

    def set_uin(self, uin):
        self._uin = uin

    def toJSON(self):
        info = {'value': self._value,
                'username': self._user,
                'origin': self._origin}
        return info

    def to_json(self):
        return json.dumps(self.toJSON())
