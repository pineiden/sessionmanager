from sessionmanager.orm.db_users import SessionGNS
from networktools.colorprint import bprint, gprint, rprint
import time
import getpass
import asyncio

loop = asyncio.get_event_loop()

session = SessionGNS()

def cli_change_passw(session):
    """
    An interactive interface to change password
    """
    action = input("Acción \n1) List Users, \n2) Check User, \n3) Change Password \n-1) Salir\n")

    bprint("==========LOGIN TEST===============")

    while not action=="-1":
        if action == "1":
            users = session.get_users()
            [bprint(user) for user in users]
        elif action == "2":
            gprint("Checking user")
            username = input("Dame el usuario\n")
            passw = getpass.getpass("Dame la pass\n")
            result, user = session.verify_password(username, passw)
            bprint("Checked password:::>")
            print(result, user)
            if result:
                print("Password Correcta")
            else:
                print("Password Incorrecta")
        elif action == "3":
            username = input("Dame el usuario\n")
            orig_passw = getpass.getpass("Dame la pass original\n")
            passw1 = getpass.getpass("Dame la nueva pass\n")
            passw2 = getpass.getpass("Repite la misma la nueva pass\n")
            instance = session.get_user(username)
            result, rb = None, None
            if instance:
                user = instance.name
                result, rb = session.change_password(user,
                                                     orig_passw,
                                                     passw1,
                                                     passw2)
            else:
                print("No existe usuario con ese nombre")

            if rb:
                print("Se cambió la password existosamente")
            else:
                print("Las password no son iguales")

        action = input("Acción \n1) List Users, \n2) Check User, \n3) Change Password \n-1) Salir\n")

cli_change_passw(session)

loop.close()
