from sessionmanager.orm.token import Token
from sessionmanager.orm.tokenmanager import TokenManager
from networktools.colorprint import gprint, bprint, rprint

#
import csv
import asyncio


# test tokens

file = './list_names'

with open(file, 'r') as f:
    reader = csv.DictReader(f)
    for row in reader:
        print(row)
        token = Token(**row)
        bprint(token)

rprint("=====TEST MANAGER====")
# init manager


async def test_tmanager(file):
    tm = TokenManager()
    name_origin = {}
    with open(file, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            origin = row.get('origin')
            name = row.get('name')
            name_origin.update({name:origin})
            await tm.generate_token(origin, name)
            tlist = tm.get_list()
            [gprint(token) for token in tlist]
            rprint("-"*64)
    def add(origin, name):
        tm.generate_token(origin, name)
    def renew(origin, name):
        tm.renew_token(origin, name)
    def delete(token):
        tm.delete_token(token)
    options = {
        'add': add,
        'renew': renew,
        'delete': delete

    }
    option = input('Give me option %s\n' % list(options.keys()))
    while not option == '-1':
        if option == 'add':
            name = input('Name\n')
            origin = input('Origin\n')
            await tm.generate_token(origin, name)
            tlist = tm.get_list()
            [gprint(token) for token in tlist]
        elif option == 'renew':
            name = input('Name\n')
            origin = input('Origin\n')
            await tm.renew_token(origin, name)
            tlist = tm.get_list()
            [gprint(token) for token in tlist]
        elif option == 'delete':
            bprint("===List of names===")
            [rprint(user) for user in name_origin]
            name = input('Selecciona Nombre\n')
            origin = name_origin.get(name)
            token = tm.gettoken(origin, name)
            bprint("To delete")
            gprint(origin)
            rprint(token)
            options.get(option)(token)
            if name in name_origin:
                name_origin.pop(name)
        option = input('Give me option %s\n' % list(options.keys()))


loop = asyncio.get_event_loop()

loop.run_until_complete(test_tmanager(file))
