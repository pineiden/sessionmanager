from sessionmanager.orm.db_users import SessionGNS
from networktools.colorprint import bprint, gprint, rprint
import time
import getpass

session = SessionGNS()


def login(session, username, password, *args):
    print("Doing login")
    gprint([username, password])
    credentials = {
        'action':'login',
        'args':[username, password]
    }
    bprint("Credentials")
    rprint(credentials)
    return session.filter(credentials)

def get_info(session, token, username, *args):
    credentials = {
        'action':'get_info',
        'token': token,
        'args':[username]
    }
    return session.filter(credentials)

def get_groups(session, token, *args):
    credentials = {
        'action':'get_groups',
        'token': token,
        'args':[]
    }
    return session.filter(credentials)

def change_password(session, token, username, passw0, newpass1, newpass2, *args):
    credentials = {
        'action':'change_password',
        'token': token,
        'args':[username, passw0, newpass1, newpass2,]
    }
    return session.filter(credentials)

def delete_user(session, token, username, *args):
    ## check authorization
    ## maybe ask token, then check user group
    credentials = {
        'action':'delete_user',
        'token': token,
        'args':[username]
    }
    return session.filter(credentials)

def create_user(session, token, username, password, password2, level_group):
    credentials = {
        'action': 'create_user',
        'token': token,
        'args': [username, password, password2, level_group]
    }
    return session.filter(credentials)

def update_user(session, token, username, fieldname, value,* args):
    credentials = {
        'action': 'update_user',
        'token': token,
        'args': [username, fieldname, value]
    }
    return session.filter(credentials)

# define functions related to selection, and ask about the args

def list_users(session, token):
    credentials = {
        'action': 'list_users',
        'token': token,
        'args': []
    }
    return session.filter(credentials)

def get_user(session, token, username):
    credentials = {
        'action': 'get_user',
        'token': token,
        'args': [username]
    }
    return session.filter(credentials)

def send_msg(session, token, username, msg):
    credentials = {
        'action': 'send_msg',
        'token': token, 
        'args': [username, token, msg]
    }
    return session.filter(credentials)


def renew_token(session, token, username, *args):
    credentials = {
        'action': 'renew_token',
        'token': token,
        'args': [username, token]
    }
    return session.filter(credentials)

def logged(tokens, *args):
    [bprint("%s -> %s" %(user, tokens.get(user))) for user in tokens]
    return None, None

def logout(session, token, username, tokens, *args):
    print("LOGOUT")
    bprint(tokens)
    tokens.pop(username)
    credentials = {
        'action': 'logout',
        'token': token,
        'args': [username]
    }
    return session.filter(credentials)

def set_user(username, tokens):
    if username in tokens:
        return username, True
    else:
        return None, None

def listprint(callback, contentlist):
    [callback(elem) for elem in contentlist]

rprint("="*30)
bprint("Pruebas para filtro de sesión")
rprint("="*30)

rprint("Realiza LOGIN antes de realizar una acción\n")
rprint("Recibirás un token de sesión\n")

bprint("Opciones posibles :>")

action_list = {
    'login': login,
    'logged': logged,
    'logout': logout,
    'list_users': list_users,
    'get_info': get_info,
    'change_password': change_password,
    'delete_user': delete_user,
    'create_user': create_user,
    'update_user': update_user,
    'get_user': get_user,
    'send_msg': send_msg,
    'renew_token': renew_token,
    'set_user': set_user,
    'get_groups': get_groups
}

[rprint("%s) %s" %(index, option)) for index, option in enumerate(list(action_list.keys()))]
action = input("Ingresa tu opcion\n")

### TODO:
# Select different users logged
# So select tokens
###

tokens = {}
active_user = None

while not action == '-1':
    if action in action_list:
        if action == 'login':
            username = input("Username:\n")
            password = getpass.getpass("Password:\n")
            result, credentials = action_list.get(action)(session, username, password)
            bprint("===RESULT====")
            rprint(result)
            rprint(credentials)
            if credentials.get('token'):
                active_user = username
                tokens.update({username: credentials.get('token')})
        elif action == 'logged':
            action_list.get(action)(tokens)
        elif action == 'list_users':
            token = tokens.get(active_user)
            result, credentials = action_list.get(action)(session, token)
            bprint("<===== Users ====>")
            listprint(rprint, result)
            rprint(credentials)
        elif action == 'logout':
            token = tokens.get(active_user)
            bprint("Pre LOGOUT")
            action_list.get('logged')(tokens)
            username = input("Username:\n")
            if username in tokens:
                result, credentials = action_list.get(action)(session,
                                                              token,
                                                              username,
                                                              tokens)
                if username == active_user:
                    active_user = None
                    gprint("Remember to set active user")
            bprint("Post LOGOUT")
            action_list.get('logged')(tokens)
        elif action == 'set_user':
            username = input("Username: \n")
            active_user, result = action_list.get(action)(username, tokens)
            bprint("Active User is %s now" %active_user)
        elif action == 'get_info':
            username = input("Username:\n")
            token = tokens.get(active_user)
            result, credentials = action_list.get(action)(session, token, username)
            bprint("===RESULT====")
            bprint("Level:>:>")
            rprint(result.get('level'))
            bprint("Commands:>:>")
            listprint(rprint, result.get('commands'))
            rprint(credentials)
        elif action == 'change_password':
            token = tokens.get(active_user)
            username = input("Username:\n")
            password0 = getpass.getpass("Original Password:\n")
            password1 = getpass.getpass("New Password:\n")
            password2 = getpass.getpass("Again, new Password:\n")
            result, credentials = action_list.get(action)(session,
                                                          token,
                                                          username,
                                                          password0,
                                                          password1,
                                                          password2)
            bprint("===RESULT====")
            rprint(result)
            rprint(credentials)
        elif action == 'create_user':
            token = tokens.get(active_user)
            username = input("Username:\n")
            level_group = input("Level group:\n")
            password0 = getpass.getpass("Original Password:\n")
            password1 = getpass.getpass("Again, original Password:\n")
            if password0==password1:
                result, credentials = action_list.get(action)(session,
                                                              token,
                                                              username,
                                                              password0,
                                                              password1,
                                                              int(level_group))
                bprint("===RESULT CREATE USER====")
                rprint(result)
                rprint(credentials)
            if not result:
                print("Las password enviada no coinciden")
        elif action == 'delete_user':
            token = tokens.get(active_user)
            username = input("Username to delete:\n")
            result, credentials = action_list.get(action)(session,
                                                          token,
                                                          username)
            bprint("===RESULT====")
            rprint(result)
        elif action == 'update_user':
            token = tokens.get(active_user)
            username = input("Username to modify:\n")
            fieldname = input("Input fieldname:\n")
            if fieldname == 'name':
                value = input("Insert newname:\n")
                result, credentials = action_list.get(action)(session,
                                                              token,
                                                              username,
                                                              [fieldname],
                                                              [value],)
            elif fieldname == 'password':
                password0 = getpass.getpass("Original Password:\n")
                password1 = getpass.getpass("New Password:\n")
                password2 = getpass.getpass("Again, new Password:\n")
                result, credentials = action_list.get('change_password')(session,
                                                              token,
                                                              username,
                                                              password0,
                                                              password1,
                                                              password2)
        elif action == 'get_user':
            token = tokens.get(active_user)
            username = input("Obtain info from this username:\n")
            result, credentials = action_list.get(action)(session,
                                                          token,
                                                          username,)
            bprint("===RESULT====")
            rprint(repr(result))
        elif action == 'send_msg':
            token = tokens.get(active_user)
            mensaje = input("Enviar mensaje:\n")
            msg = {'msg':mensaje}
            result, credentials = action_list.get(action)(session,
                                                          token,
                                                          active_user,
                                                          msg)
            bprint("===RESULT====")
            rprint(repr(result))
        elif action == 'renew_token':
            token = tokens.get(active_user)
            result, credentials = action_list.get(action)(session,
                                                          token,
                                                          active_user)
            bprint("===RESULT====")
            rprint(repr(result))
        else:
            bprint("Action %s is not defined yet" %action)
    else:
        bprint("Unknown action, use the list")
    # again, ask action
    bprint("Opciones posibles :>")
    [rprint("%s) %s" %(index, option)) for index, option in enumerate(list(action_list.keys()))]
    action = input("Ingresa tu opcion\n")



