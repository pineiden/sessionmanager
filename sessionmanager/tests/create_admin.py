from sessionmanager.orm.db_users import SessionGNS
from networktools.colorprint import bprint, gprint, rprint
import time
import getpass
import asyncio

session = SessionGNS()

async def cli(session):
    """
    A interface to create admin user, check password, modify user

    """
    rprint("="*30)
    bprint("Añadir un usuario admin")
    rprint("="*30)

    user = ('david', 'GPS2018', 1)

    exist_user = await session.get_user(user[0])
    if not exist_user:
        print("Usuario no existe, creando")
        exist_user = await session.user(*user)
        print("=====")
        bprint(exist_user)
    else:
        print("Usuario existe")
        exist_user = exist_user[0]
        print(exist_user)

    rprint("="*30)
    bprint("Check usuario admin first password")
    rprint("="*30)

    user = input("Dame el usuario\n")
    passw = getpass.getpass("Dame la pass\n")

    result, user = await session.verify_password(user, passw)

    print(result, user)

    if result:
        print("Password Correcta")
    else:
        print("Password Incorrecta")

    rprint("="*30)
    bprint("Modificar usuario admin: david")
    rprint("="*30)

    newname = input("Dame un nuevo nombre\n")
    password = getpass.getpass("Dame la pass\n")

    bprint("Exist user before")
    bprint(exist_user)

    await session.update_user(exist_user.name, password,['name'], [newname])

    exist_user = newname

    rprint("="*30)
    bprint("Modificar usuario password")
    rprint("="*30)

    newname = input("Dame nombre de usuario\n")
    opass = getpass.getpass("Dame la password original\n")
    newpass1 = getpass.getpass("Dame la nueva pass\n")
    newpass2 = getpass.getpass("Dame la nueva pass de nuevo\n")

    instance = await session.get_user(exist_user)

    result, msg = await session.change_password(instance.name, opass, newpass1, newpass2)

    if result:
        print("Password cambiada correctamente")
    else:
        print(msg)

    rprint("="*30)
    bprint("Check usuario admin %s" %newname)
    rprint("="*30)

    user = input("Dame el usuario\n")
    passw = getpass.getpass("Dame la pass\n")

    result, user = await session.verify_password(user, passw)

    print(result, user)

    if result:
        print("Password Correcta")
    else:
        print("Password Incorrecta")


loop = asyncio.get_event_loop()
loop.run_until_complete(cli(session))

loop.close()
