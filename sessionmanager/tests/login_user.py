from sessionmanager.orm.db_users import SessionGNS
from networktools.colorprint import bprint, gprint, rprint
import time
import getpass
import asyncio

loop = asyncio.get_event_loop()
session = SessionGNS()


def shutdown(loop):
    for task in asyncio.Task.all_tasks():
        print("Canceling task")
        bprint(task)
        task.cancel()


async def login_user(session):
    action = None
    while not action=="-1":
        bprint("==========LOGIN TEST===============")
        user = input("Dame el usuario\n")
        passw = getpass.getpass("Dame la pass\n")
        result, user = await session.verify_password(user, passw)
        print(result, user)
        if result:
            print("Password Correcta")
        else:
            print("Password Incorrecta")
        action = input("nueva acción? (cerrar con -1)")
        session.close()


loop.run_until_complete(login_user(session))
shutdown(loop)
loop.close()
