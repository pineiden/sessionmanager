from sessionmanager.orm.db_users import SessionGNS
from networktools.colorprint import bprint, gprint, rprint
import time
import getpass
import asyncio

session = SessionGNS()


rprint("="*30)
bprint("Añadir un usuario o revisar")
rprint("="*30)



async def cli(session):
    action = input("Acción \n1) Create User, \n2) Check User, \n-1) Salir\n")
    while not action == '-1':
        username = input("Username\n")
        passw = getpass.getpass("Dame la pass\n")
        
        if action == "1":
            passw2 = getpass.getpass("Dame la pass de nuevo\n")
            group = input("Group \n1) admin \n2) analyst \n3) visitor\n")
            user = (username, passw, passw2, group)
            exist_user = await session.new_user(*user)
            if not exist_user:
                print("Crando usuario ...")
                exist_user = session.user(*user)
                print(exist_user)
            else:
                print("Usuario existe")
                exist_user = exist_user
                print(exist_user)

        if action == "2":
            result, user = await session.verify_password(username, passw)
            bprint("Checked password:::>")
            print(result, user)
            if result:
                print("Password Correcta")
            else:
                print("Password Incorrecta")

        action = input("Acción \n1) Create User, \n2) Check User, \n-1) Salir\n")

loop = asyncio.get_event_loop()

loop.run_until_complete(cli(session))

loop.close()
