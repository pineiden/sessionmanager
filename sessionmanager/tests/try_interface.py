from sessionmanager.orm.interface import SessionInterface
from networktools.colorprint import bprint, gprint, rprint
import time
import getpass
import asyncio

interface = SessionInterface()


async def cli(interface):
    d_actions = interface.get_actions()
    [rprint("%s) %s" %(index, option)) for index, option in enumerate(
        list(d_actions.keys()))]
    action = input("Ingresa tu opcion\n")

    ### TODO:
    # Select different users logged
    # So select tokens
    ###

    tokens = {}
    active_user = None

    while not action == '-1':
        bprint("====List of tokens=====")
        rprint(interface.session.token_manager.get_list())
        bprint("ACTION :> %s" %action)
        await interface.term_input(action, origin='test000')
        [rprint("%s) %s" %(index, option)) for index, option in enumerate(
            list(d_actions.keys()))]
        action = input("Ingresa tu opcion\n")

loop = asyncio.get_event_loop()

loop.run_until_complete(cli(interface))

loop.close()

