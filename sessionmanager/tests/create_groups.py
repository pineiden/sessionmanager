from sessionmanager.orm.db_users import SessionGNS
import asyncio

"""
Create groups with commands associated

"""

levels = [
    (1, 'admin', 'manage and control all the information system','111','111', 'NO'),
    (2, 'analyst', 'view and obtain information and data from the system','011','111', 'Si'),
    (3, 'visitor', 'any person that whant to view the general information of the system','001','011', 'Si')
]

commands =  [
    ('create', 'create a new user', 1),
    ('read', 'read or view data from users', 3),
    ('update', 'modify info from user', 2),
    ('delete', 'delete all data from user', 2),
]

session = SessionGNS()

async def cli(session, levels, commands):
    """
    Save data from levels and groups to database

    """
    for level in levels:
        print("Creating level",level)
        session.level(*level)

    for command in commands:
        print("Creating command associated a level %s",command)
        session.command(*command)


loop = asyncio.get_event_loop()
loop.run_until_complete(cli(session, levels, commands))

loop.close()
