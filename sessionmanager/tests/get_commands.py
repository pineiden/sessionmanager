from sessionmanager.orm.db_users import SessionGNS
from networktools.colorprint import bprint, gprint, rprint
import time

session = SessionGNS()


rprint("="*30)
bprint("Obtener comandos por nivel")
rprint("="*30)


action = input("Acción \n1) Show Levels, \n2) Get Commands by Level, \n-1) Salir\n")

max_level = 0
while not action == '-1':
    if action == "1":
        levels = session.get_levels()
        [bprint(level) for level in levels]
        values = [level.value for level in levels]
        max_level = max(values)
    elif action == "2":
        level = input("Dame un nivel\n")
        level_info_list = []
        if level.isdigit():
            for lev in range(int(level), max_level+1):
                level_info = session.get_level_by_id(lev)
                level_info_list.append(level_info)
        else:
            level_info = session.get_level_by_id(1)
        print("Level info")
        print(level_info_list)
        for level_info in level_info_list:
            [rprint(command) for command in level_info.commands]
    action = input("Acción \n1) Show Levels, \n2) Get Commands by Level, \n-1) Salir\n")


