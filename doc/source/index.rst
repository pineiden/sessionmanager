.. Session Manager documentation master file, created by
   sphinx-quickstart on Thu Oct 18 13:31:10 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Session Manager's documentation!
===========================================

This project is created to be used with systems that require to manage users with different levels of authorization.

The main classes work as message's filter to control what can an user do or not.

We provide the methods to create users, login, modify. Also define a set of levels of authorization and commands.

Also there are a command line script to manage the data using the terminal.

The system control the user password with high security's standar, using *argon2* to encrypt and the *salt* (see passsalt_) added to the password string. With that is hard to break out the system and, at the end, is more safe.

.. _passsalt: https://en.wikipedia.org/wiki/Salt_(cryptography)

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents:

   content/install
   content/dbsqlite
   content/dbpostgres
   content/saltpepper
   content/codedoc
   content/accesscontrol


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
