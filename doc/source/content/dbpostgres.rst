Using Postgresql
================

This database system is required as standar

You have to define, on the *postactivate* file these variables:

.. code-block:: bash

   export UM_USER=[USER]
   export UM_PASSW=[PASSW]
   export UM_HOST=[HOST]
   export UM_PORT=[PORT_DB]
   export UM_DBNAME=[DBNAM]


Are the variables from the database created to manage our users.

