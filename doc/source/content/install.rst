.. toctree::
   :maxdepth: 2


Installing the Module
=====================

You have to clone the project_ from the *CSN Gitlab* platform. Then active the *virtualenv*, install the package and requeriments.


.. code-block:: bash

   git clone http://gitlab.csn.uchile.cl/dpineda/sessionmanager.git
   cd sessionmanager
   workon sessionmanager_env
   python setupy.py develop
   pip install -r requeriments.txt

You also have to install the Rethinkdb database, with debian, see rethinkdb_. Then run rethinkdb

.. code-block:: bash
   rethinkdb &

Also, is recommended yo manage rethinkdb with some more security, disabling the interface port *8080* to outsiders or set the interface port on the reserved tranche of values (0 to 1024)


.. _project: http://gitlab.csn.uchile.cl/dpineda/sessionmanager
.. _rethinkdb: https://www.rethinkdb.com/docs/install/debian/
