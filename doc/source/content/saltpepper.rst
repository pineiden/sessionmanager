Salt Pepper Module
==================

The Salt Pepper module is used to manage safely the passwords. Is implemented on the SessionGNS class, and require to be used with *Rethinkdb* running.

Also, you have to install on the *environment* these modules

.. code-block:: bash

   outcome==1.0.0
   psycopg2-binary==2.7.5
   Represent==1.5.1
   rethinkdb==2.3.0.post6
   simplejson==3.8.2
   six==1.11.0
   SQLAlchemy==1.2.11
   termcolor==1.1.0
