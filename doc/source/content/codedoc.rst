Code's Documentation
====================

These documentation came from the source code.

--------------
SessionManager
--------------

Those are the classes implemented for the SessionManager

^^^^^^^^
Password
^^^^^^^^

.. autofunction:: sessionmanager.orm.password.get_rounds

.. autoclass:: sessionmanager.orm.password.PasswordHash
   :members:


^^^^^^^^^^^^^^^
Column Password
^^^^^^^^^^^^^^^

.. autoclass:: sessionmanager.orm.columns.Password
   :members:



^^^^^^^^^^^
Model Level
^^^^^^^^^^^

.. autoclass:: sessionmanager.orm.models.Level
   :members:


^^^^^^^^^^
Model User
^^^^^^^^^^

.. autoclass:: sessionmanager.orm.models.User
   :members:


^^^^^^^^^^^^^
Model Command
^^^^^^^^^^^^^

.. autoclass:: sessionmanager.orm.models.Command
   :members:


^^^^^^^^^^^^^^^
Session Manager
^^^^^^^^^^^^^^^

.. autoclass:: sessionmanager.orm.db_session.ManagerSession
   :members:


^^^^^^^^^^^^^^^
Create Database
^^^^^^^^^^^^^^^

.. autofunction:: sessionmanager.orm.create_db.create_db


^^^^^^^^^^^^^^^^^^^
User Manager Sesion
^^^^^^^^^^^^^^^^^^^


.. autoclass:: sessionmanager.orm.db_users.SessionHandle
   :members:

.. autoclass:: sessionmanager.orm.db_users.SessionGNS
   :members:

^^^^^^^^^
Interface
^^^^^^^^^
.. autoclass:: sessionmanager.orm.interface.SessionInterface
   :members:

Tests
-----

Create Groups
^^^^^^^^^^^^^
Create levels and groups

Change Password
^^^^^^^^^^^^^^^
Change password from user

Create Admin
^^^^^^^^^^^^
Create user admin and check password, modify passw, modify user.

Command Line
------------

.. autofunction:: sessionmanager.commandline.interface.cli

