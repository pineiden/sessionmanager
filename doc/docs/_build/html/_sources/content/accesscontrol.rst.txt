Access Control on Session Manager
=================================

There is defined a simple and hierarchical access control methods.

On the first level:

1. If you can read or write

On the second level:

1. If you can access or not to same level elements

2. If you can access or not to itself

3. If you can access or not to lower levels

On the models, the groups are defined with a string with 3 chars {000,001,010,011,100,101,110,111}
