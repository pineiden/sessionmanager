from setuptools import setup

setup(name='sessionmanager',
      version='0.1.0',
      description='Session Manager ORM module to manage and control users, sessions, etc',
      url='',
      author='David Pineda Osorio',
      author_email='dpineda@uchile.cl',
      license='MIT',
      packages=[],
      zip_safe=False)
